﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Zawodnicy.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class SandboxTajneController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "Tajne dane z controllera SandboxTajneController w Api";
        }
    }
}
