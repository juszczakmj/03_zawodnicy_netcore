﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Zawodnicy.Infrastructure.Commands;
using Zawodnicy.Infrastructure.DTO;
using Zawodnicy.Infrastructure.Services;

namespace Zawodnicy.API.Controllers
{
    [Route("[Controller]")]
    public class SandboxZawodnicyApiController : Controller
    {
        private readonly IZawodnicyService _zawodnicyService;

        public SandboxZawodnicyApiController(IZawodnicyService zawodnicyService)
        {
            _zawodnicyService = zawodnicyService;
        }

        [HttpGet]       //ROdzaj Get==tylko jedna metoda Get, inaczej jak niżej zmiany ściezki
                        //nazwa metody nie ma znaczenia
        public async Task<ActionResult> GetZawodnik(int id)
        {
            ZawodnikDto z = await _zawodnicyService.GetAsync(id);
            return Json(z);
        }

        //jeśli w nazwiasach klamrowych to przekazanie argumentów do ścieżki
        //http://localhost:5000/ZawodnicyAPI/jan/kowalski
        [HttpGet("{nazwisko}/{imie}")]  //zgodne z parametrami metody - rozróznienie innego Get
        //takze po to żeby rozrożnic jesli dwie metody bedą get
        public async Task<ActionResult> Get(string imie, string nazwisko)
        {
            ZawodnikDto z = await _zawodnicyService.GetAsync(1);
            return Json(imie + " " + nazwisko);
        }

        [HttpPost]
        //public async Task<ActionResult> Create(string imie,string nazwisko)
        public async Task<ActionResult> Create([FromBody] CreateZawodnik z)
        {
            return Json("Tworze zawodnika" + z.Imie + " " + z.Nazwisko);
        }

        //inna ścieżka metody Post
        //Nazwa ścieżki na Inna
        //http://localhost:5000/ZawodnicyAPI/Inna
        [HttpPost("Inna")]
        public async Task<ActionResult> InnaMetodaCreate([FromBody] CreateZawodnik z) 
        {
            return Json("Tworze zawodnika Inna Metoda: " + z.Imie + " " + z.Nazwisko);
        }

        //nazwa ścieżki zgodna z nazwą metody CokolwiekCreate
        //Action - bierze nazwę z metody - w wywołaniu jest nazwa metody w ścieżce
        //http://localhost:5000/ZawodnicyAPI/CokolwiekCreate
        [HttpPost("[Action]")]
        public async Task<ActionResult> CokolwiekCreate([FromBody] CreateZawodnik z)
        {
            return Json("Tworze zawodnika Cokolwiek: " + z.Imie + " " + z.Nazwisko);
        }

    }
}
