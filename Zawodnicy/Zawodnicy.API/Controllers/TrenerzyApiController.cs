﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Zawodnicy.Infrastructure.Commands;
using Zawodnicy.Infrastructure.DTO;
using Zawodnicy.Infrastructure.Services;

namespace Zawodnicy.API.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public class TrenerzyApiController : Controller
    {
        private readonly ITrenerzyService _trenerzyService;
        public TrenerzyApiController(ITrenerzyService trenerzyService)
        {
            _trenerzyService = trenerzyService;
        }

        // GET: <TrenerzyApiController>
        //default path TrenerzyApi
        [HttpGet]
        public async Task<ActionResult> BrowseAll()
        {
            IEnumerable<TrenerDto> t = await _trenerzyService.BrowseAll();
            List<TrenerDto> tlista = t.ToList();
            return Json(tlista);
        }

        // GET <TrenerzyApiController>/5
        // path TrenerzyApi/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult> GetAsync(int id)
        {
            TrenerDto t = await _trenerzyService.GetAsync(id);
            return Json(t);
        }

        //Sciezka z argumentem TrenrzyApi/filtr?Imie=imie - tu nie wykorzystujemy
        //cokolwiek trzeba dodać po HttpGet (tutaj filtr) bo inaczej zawsze wejdzie do defaultowego Get
        [HttpGet("filtr")]
        public async Task<ActionResult> BrowseAsync(string imie, string nazwisko)
        {
            return Json(_trenerzyService.BrowseAsync(imie,nazwisko));
        }

        // POST <TrenerzyApiController>
        // path default TrenerzyApi
        // CreateTrener - Command w Infrastructure, okrslamy tam co przekazujemy w Json jako FromBody
        [HttpPost]
        public async Task<ActionResult> AddAsync([FromBody] CreateTrener trener)
        {
            await _trenerzyService.AddAsync(new TrenerDto() { 
                Imie=trener.Imie,
                Nazwisko=trener.Nazwisko,
                Team=trener.Team
            });

            //na razie - pamiętać żeby AddAsync w TrenerzyResitory zmienić tak żeby zwracało nam 
            //id nowo utworzonego usera
            //wtedy zwrócimy pełną ścieżkę w rodzaju wywołania get do wywołania i 
            //otrzymania usera np trenerzyApi/4 
            string s = string.Format("trenerzyApi/{0}", 4);

            return Created(s, null); //status hhtp - created
            
        }

        // PUT <TrenerzyApiController>/5
        // path TrenerzyApi/{id}
        // w update zwracamy nocontent zamiast created
        // UpdateTrener - Command w Infrastructure, okrslamy tam co przekazujemy w Json jako FromBody
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateTrener trener, int id)
        {
            trener.Id = id;
            await _trenerzyService.UpdAsync(new TrenerDto() { 
                Id=trener.Id,
                Imie=trener.Imie,
                Nazwisko=trener.Nazwisko,
                Team=trener.Team
            });

            return NoContent();
        }

        // DELETE <TrenerzyApiController>/5
        // path TrenerzyApi/{id}
        //w update zwracam nocontent zamiast created
        [HttpDelete("{id}")]
        public async Task<ActionResult> DelAsync(int id)
        {
            await _trenerzyService.DelAsync(new TrenerDto() { Id=id});
            return NoContent();
        }
    }
}
