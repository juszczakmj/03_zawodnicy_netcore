﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Zawodnicy.Core.Domain;
using Zawodnicy.Infrastructure.Commands;
using Zawodnicy.Infrastructure.DTO;
using Zawodnicy.Infrastructure.Services;

namespace Zawodnicy.API.Controllers
{
    [Route("[Controller]")]
    [Authorize]
    public class ZawodnicyApiController : Controller
    {
        private readonly IZawodnicyService _zawodnicyService;
        public ZawodnicyApiController(IZawodnicyService zawodnicyService)
        {
            _zawodnicyService = zawodnicyService;
        }

        //sciezka defaultowa
        //sciezka ZawodnicyApi
        [HttpGet]
        public async Task<IActionResult> BrowseAll()
        {
            //List<ZawodnikDto> z = _zawodnicyService.BrowseAll().Result.ToList();
            //lub
            IEnumerable<ZawodnikDto> z = await _zawodnicyService.BrowseAll();
            List<ZawodnikDto> zlista = z.ToList();

            return Json(zlista);
        }


        //sciezka ZawodnicyApi/{id}
        //[HttpGet("{id}")]
        //OK, ale można krócej
        //public async Task<IActionResult> GetAsync(int id)
        //{
        //    ZawodnikDto z=await _zawodnicyService.GetAsync(id);
        //    return Json(z);
        //}

        //sciezka ZawodnicyApi/{id}
        //krócej
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            ZawodnikDto z = await _zawodnicyService.GetAsync(id);
            return Json(z);
        }

        //Sciezka z argumentem ZawodnicyApi/filtr?Imie=imie
        //cokolwiek trzeba dodać po HttpGet (tutaj filtr) bo inaczej zawsze wejdzie do defaultowego Get
        [HttpGet("filtr")]
        public async Task<ActionResult> BrowseAsync(string imie, string nazwisko)
        //=> await Task.FromResult(Json(_zawodnicyService.BrowseAsync(imie)));
        {
            return Json(_zawodnicyService.BrowseAsync(imie,nazwisko));
        }
        

        ////sciezka ZawodnicyApi
        ///CreateZawodnik - Command w Infrastructure, okrslamy tam co przekazujemy w Json jako FromBody
        [HttpPost]
        public async Task<IActionResult> AddAsync([FromBody] CreateZawodnik zawodnik) 
        {
            await _zawodnicyService.AddAsync(new ZawodnikDto() { 
                Imie = zawodnik.Imie, 
                Nazwisko = zawodnik.Nazwisko, 
                Wzrost=zawodnik.Wzrost, 
                DataUr=zawodnik.DataUr, 
                IdTrenera=zawodnik.IdTrenera 
            });

            //na razie - pamiętać żeby AddAsync w ZawodnicyResitory zmienić tak żeby zwracało nam 
            //id nowo utworzonego usera
            //wtedy zwrócimy pełną ścieżkę w rodzaju wywołania get do wywołania i 
            //otrzymania usera np zawodnicyApi/4 
            string s = string.Format("zawodnicyApi/{0}",4);

            return Created(s,null); //status hhtp - created==stworzone
            //przekazujemy pełną ścieżkę dostępu do nowo utworzonego zasobu == get
            //sa tez deleted, updated

        }

        //w update zwracam nocontent zamiast created
        //scieżka ZawodnicyAPI/{id}
        //UpdateZawodnik - Command w Infrastructure, okrslamy tam co przekazujemy w Json jako FromBody
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync([FromBody] UpdateZawodnik zawodnik, int id)
        {
            zawodnik.Id = id;
            await _zawodnicyService.UpdAsync(new ZawodnikDto() { 
                Id=zawodnik.Id, 
                Imie=zawodnik.Imie,
                Nazwisko=zawodnik.Nazwisko,
                Wzrost = zawodnik.Wzrost,
                DataUr = zawodnik.DataUr,
                IdTrenera = zawodnik.IdTrenera
            });

            return NoContent();
        }

        //w update zwracam nocontent zamiast created
        //scieżka ZawodnicyAPI/{id}      
        [HttpDelete("{id}")]
        public async Task<IActionResult> DelAsync(int id)
        {
            await _zawodnicyService.DelAsync(new ZawodnikDto() { Id = id });      

            return NoContent();
        }


    }
}
