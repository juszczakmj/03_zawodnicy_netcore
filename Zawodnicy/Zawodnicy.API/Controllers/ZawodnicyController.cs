﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Zawodnicy.Core.Domain;
using Zawodnicy.Core.Repositories;
using Zawodnicy.Infrastructure.DTO;
using Zawodnicy.Infrastructure.Services;

namespace Zawodnicy.API.Controllers
{
    [Route("[Controller]")]
    public class ZawodnicyController:Controller
    {

        private readonly IZawodnicyService _zawodnikService;
        
        public ZawodnicyController(IZawodnicyService zawodnikService)
        {
            _zawodnikService = zawodnikService;
        }

        public ActionResult Index()
        {
            var x = _zawodnikService.BrowseAll();
            HashSet<ZawodnikDto> ZawodnicyDto = new HashSet<ZawodnikDto>();

            foreach (var item in x.Result)
            {
                ZawodnicyDto.Add(new ZawodnikDto() { Imie=item.Imie, Nazwisko=item.Nazwisko});
            }


            return View(ZawodnicyDto);
            //return View();
            //return Json("Hej!");
        }

    }
}
