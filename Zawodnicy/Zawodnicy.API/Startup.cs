using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Zawodnicy.Core.Repositories;
using Zawodnicy.Infrastructure.Services;
using Zawodnicy.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;

namespace Zawodnicy.API
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        public IConfiguration Configuration;

        public Startup(IConfiguration confiruration)
        {
            Configuration = confiruration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            //dodanie service obs�ugi Zawodnicy
            services.AddScoped<IZawodnicyRepository, ZawodnicyRepository>();
            services.AddScoped<IZawodnicyService, ZawodnicyService>();
            //dodanie service obs�ugi Trenerzy
            services.AddScoped<ITrenerzyRepository, TrenerzyRepository>();
            services.AddScoped<ITrenerzyService, TrenerzyService>();

            services.AddMvc();

            //dostep do bazy
            //ConnectionString w appsettings.json - dostep poprzez pole Configuration
            services.AddDbContext<AppDbContext>(
                options => options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            //JSON WebToken
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
              .AddJwtBearer(options =>
              {
                  options.SaveToken = true;
                  options.RequireHttpsMetadata = false;
                  options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                  {
                      ValidateIssuer = true,
                      ValidateAudience = true,
                      ValidAudience = Configuration["RestApiUrl:HostUrl"],
                      ValidIssuer = Configuration["RestApiUrl:HostUrl"],
                      IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Secret:SecretKey"]))
                  };
              });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            //JSON Web Token
            app.UseAuthentication();
            app.UseAuthorization();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapGet("/", async context =>
            //    {
            //        await context.Response.WriteAsync("Hello World!");
            //    });
            //});

            //ruting z domy�lnymi �cie�kami controller/metoda/indeks
            app.UseEndpoints(endpoit =>
            {
                endpoit.MapControllers();
            });
        }
    }
}
