﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zawodnicy.Core.Domain
{
    public class Trener
    {
        public Trener()
        {
                
        }
        public Trener(int id)
        {
            Id = id;
        }

        public int Id { get; protected set; }
        public string Imie { get; protected set; }
        public string Nazwisko { get; protected set; }
        public string Team { get; protected set; }
        public void UstawImieNazwiskoTeam(string imie, string nazwisko, string team)
        {
            Imie = imie;
            Nazwisko = nazwisko;
            Team = team;
        }
    }
}
