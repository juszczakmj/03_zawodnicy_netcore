﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zawodnicy.Core.Domain
{
    public class Zawodnik
    {
        public Zawodnik()
        {

        }
        public Zawodnik(int id)
        {
            Id = id;
        }

        public int Id { get; protected set; }
        public string Imie { get; protected set; }
        public string Nazwisko { get; protected set; }
        public DateTime DataUr { get; protected set; }      //żeby było szybciej nieużywane w kodzie
        public float Wzrost { get; protected set; }
        public int IdTrenera { get; protected set; }        //powiazanie z Trenerami
        public void UstawImieNazwisko(string imie, string nazwisko, DateTime dataur, float wzrost, int idtrenera)
        {
            Imie = imie;
            Nazwisko = nazwisko;
            DataUr = dataur;
            Wzrost = wzrost;
            IdTrenera = idtrenera;
        }

       
    }
}
