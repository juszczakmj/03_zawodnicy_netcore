﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zawodnicy.Core.Domain;

namespace Zawodnicy.Core.Repositories
{
    public interface ITrenerzyRepository
    {
        Task<Trener> BrowseAsync(string imie, string nazwisko);
        Task AddAsync(Trener trener);
        Task UpdateAsync(Trener trener);
        Task DelAsync(Trener trener);
        Task<Trener> GetAsync(int id);
        Task<IEnumerable<Trener>> BrowseAll();

    }
}
