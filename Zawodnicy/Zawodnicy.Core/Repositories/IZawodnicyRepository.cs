﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zawodnicy.Core.Domain;

namespace Zawodnicy.Core.Repositories
{
    public interface IZawodnicyRepository
    {
        //Task<Zawodnik> BrowseAsync(string imie);
        //Zmiana - wyszukiwanie zawodnika nie tylko po imieniu, ale także po nazwisku: 
        //zmiany we wszystkich odwołaniach
        Task<Zawodnik> BrowseAsync(string imie, string nazwisko);
        Task AddAsync(Zawodnik zawodnik);
        Task UpdateAsync(Zawodnik zawodnik);
        Task DelAsync(Zawodnik zawodnik);
        Task<Zawodnik> GetAsync(int id);
        Task<IEnumerable<Zawodnik>> BrowseAll();    //all colection

    }
}
