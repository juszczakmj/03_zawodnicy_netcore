﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zawodnicy.Infrastructure.Commands
{
    public class CreateTrener
    {
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Team { get; set; }
    }
}
