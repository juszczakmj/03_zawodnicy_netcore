﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zawodnicy.Infrastructure.Commands
{
    public class CreateZawodnik
    {
        //tutaj okrslamy co przekazujemy w Json jako FromBody
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public DateTime DataUr { get; set; }
        public float Wzrost { get; set; }
        public int IdTrenera { get; set; }

    }
}
