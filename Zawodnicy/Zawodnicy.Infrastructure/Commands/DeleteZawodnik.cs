﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zawodnicy.Infrastructure.Commands
{
    public class DeleteZawodnik
    {
        //tutaj okrslamy co przekazujemy w Json jako FromBody
        public int Id { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }

    }
}
