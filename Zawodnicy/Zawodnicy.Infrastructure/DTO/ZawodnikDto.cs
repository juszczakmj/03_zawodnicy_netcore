﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zawodnicy.Infrastructure.DTO
{
    public class ZawodnikDto
    {
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public int Id { get; set; }
        public DateTime DataUr { get; set; }
        public float Wzrost { get; set; }
        public int IdTrenera { get; set; }

        //plus ewentualna walidacja

    }
}
