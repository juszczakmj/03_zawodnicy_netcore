﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Zawodnicy.Core.Domain;

namespace Zawodnicy.Infrastructure.Repositories
{
    //dostep do bazy
    public class AppDbContext : IdentityDbContext<IdentityUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options): base(options)
        {

        }
        //dodanie tabeli Zawodników
        public DbSet<Zawodnik> Zawodnicy { get; set; }
        //Dodanie tabeli Trenerów
        public DbSet<Trener> Trenerzy { get; set; }

    }

}
