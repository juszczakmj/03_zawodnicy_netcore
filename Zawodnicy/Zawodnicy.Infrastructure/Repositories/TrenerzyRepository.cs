﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Zawodnicy.Core.Domain;
using Zawodnicy.Core.Repositories;

namespace Zawodnicy.Infrastructure.Repositories
{
    public class TrenerzyRepository : ITrenerzyRepository
    {
        private AppDbContext _appDbContext;
        public TrenerzyRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task AddAsync(Trener trener)
        {
            try
            {
                _appDbContext.Trenerzy.Add(trener);
                _appDbContext.SaveChanges();
                await Task.CompletedTask;
            }
            catch (Exception ex)
            {

                //Obsługa błędu
                await Task.FromException(ex);
            }          

        }

        public async Task<IEnumerable<Trener>> BrowseAll()
        {
            return await Task.FromResult(_appDbContext.Trenerzy);
        }

        public async Task<Trener> BrowseAsync(string imie, string nazwisko)
        {
            var result = _appDbContext.Trenerzy.FirstOrDefault(x => (x.Imie == imie && x.Nazwisko == nazwisko));
            return await Task.FromResult(result);
        }

        public async Task DelAsync(Trener trener)
        {
            try
            {
                _appDbContext.Remove(_appDbContext.Trenerzy.FirstOrDefault(x => x.Id == trener.Id));
                _appDbContext.SaveChanges();

            }
            catch (Exception ex)
            {

                //obsługa błędu dostępu do danych
                await Task.FromException(ex);
            }

            await Task.CompletedTask;
        }

        public async Task<Trener> GetAsync(int id)
        {
            return await Task.FromResult(_appDbContext.Trenerzy.FirstOrDefault(x => x.Id == id));
        }

        public async Task UpdateAsync(Trener trener)
        {
            try
            {                
                var z = _appDbContext.Trenerzy.First(x => x.Id == trener.Id);
                z.UstawImieNazwiskoTeam(z.Imie, z.Nazwisko, z.Team);
                _appDbContext.SaveChanges();


            }
            catch (Exception ex)
            {
                await Task.FromException(ex);
            }

        }
    }
}
