﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Zawodnicy.Core.Domain;
using Zawodnicy.Core.Repositories;

namespace Zawodnicy.Infrastructure.Repositories
{
    public class ZawodnicyRepository : IZawodnicyRepository
    {
        //static do testowania update i create żeby nie uciekały dane z listy
        //public List<Zawodnik> ZawodnicyList => _appDbContext.Zawodnicy.ToList();

        private AppDbContext _appDbContext;
        public ZawodnicyRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public ZawodnicyRepository()
        {
            //ZawodnicyMock.Add(new Zawodnik(1));
            //ZawodnicyMock.Last().UstawImieNazwisko("Janek", "Nowak");
            //ZawodnicyMock.Add(new Zawodnik(2));
            //ZawodnicyMock.Last().UstawImieNazwisko("Anna", "Koniecpolska");
            //ZawodnicyMock.Add(new Zawodnik(3));
            //ZawodnicyMock.Last().UstawImieNazwisko("Kubuś", "Puchatek");
            //ZawodnicyMock.Add(new Zawodnik(4));
            //ZawodnicyMock.Last().UstawImieNazwisko("Koziołek", "Matołek");
            //ZawodnicyMock.Add(new Zawodnik(5));
            //ZawodnicyMock.Last().UstawImieNazwisko("James", "Bond");


        }

        public async Task AddAsync(Zawodnik zawodnik)
        {
            //throw new NotImplementedException();
            try
            {
                _appDbContext.Zawodnicy.Add(zawodnik);
                _appDbContext.SaveChanges();
                await Task.CompletedTask;
            }
            catch (Exception ex)
            {

                //throw;
                //obsługa błędu np logowanie, autoryzacja itd
                await Task.FromException(ex);
                
            }
            
        }

        public async Task<IEnumerable<Zawodnik>> BrowseAll()
        {
            //throw new NotImplementedException();
            //return await Task.FromResult(ZawodnicyList);
            //return await Task.FromResult(ZawodnicyList);
            return await Task.FromResult(_appDbContext.Zawodnicy);
        }

        public async Task<Zawodnik> BrowseAsync(string imie, string nazwisko)
        {
            //throw new NotImplementedException();
            //var result = ZawodnicyList.FirstOrDefault(x=>x.Imie==imie);
            var result = _appDbContext.Zawodnicy.FirstOrDefault(x => (x.Imie == imie && x.Nazwisko==nazwisko));
            return await Task.FromResult(result);
        }


        public async Task DelAsync(Zawodnik zawodnik)
        {
            try
            {
                //Wiemy ze zawodnik istnieje bo w ZawodnicyServiece to sprawdzilismy
                //ZawodnicyList.Remove(ZawodnicyList.FirstOrDefault(x => x.Id == zawodnik.Id));
                _appDbContext.Remove(_appDbContext.Zawodnicy.FirstOrDefault(x => x.Id == zawodnik.Id));
                _appDbContext.SaveChanges();

            }
            catch (Exception ex)
            {

                //obsługa błędu dostępu do danych
                await Task.FromException(ex);
            }

            await Task.CompletedTask;
        }

        public async Task<Zawodnik> GetAsync(int id)
        {
            //throw new NotImplementedException();
            //return await Task.FromResult(ZawodnicyList.FirstOrDefault(x => x.Id == id));
            return await Task.FromResult(_appDbContext.Zawodnicy.FirstOrDefault(x => x.Id == id));
        }

        public async Task UpdateAsync(Zawodnik zawodnik)
        {
            //throw new NotImplementedException();
            try
            {
                //tutaj już zawodnik wiemy że istnieje bo sprawdziliśmy to
                //w servises/ZawodnicyServices.cs
                //ZawodnicyList.FirstOrDefault(x => x.Id == zawodnik.Id).UstawImieNazwisko(zawodnik.Imie,zawodnik.Nazwisko);                            

                var z = _appDbContext.Zawodnicy.First(x => x.Id == zawodnik.Id);
                z.UstawImieNazwisko(z.Imie, z.Nazwisko,z.DataUr,z.Wzrost,z.IdTrenera);
                _appDbContext.SaveChanges();


            }
            catch (Exception ex)
            {

                //throw;
                //obsługa błędu dostępu do danych
                await Task.FromException(ex);
            }

            await Task.CompletedTask;

        }
    }
}
