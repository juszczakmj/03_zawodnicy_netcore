﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zawodnicy.Infrastructure.DTO;

namespace Zawodnicy.Infrastructure.Services
{
    public interface ITrenerzyService
    {
        Task<TrenerDto> BrowseAsync(string imie, string nazwisko);
        Task AddAsync(TrenerDto trenerDto);
        Task UpdAsync(TrenerDto trenerDto);
        Task DelAsync(TrenerDto trenerDto);
        Task<TrenerDto> GetAsync(int id);
        Task<IEnumerable<TrenerDto>> BrowseAll();    //all colection

    }
}
