﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zawodnicy.Core.Domain;
using Zawodnicy.Infrastructure.DTO;

namespace Zawodnicy.Infrastructure.Services
{
    public interface IZawodnicyService
    {
        //tutaj poza metodami analog do IZawodicyRepository też inne metody np send mail
        Task<ZawodnikDto> BrowseAsync(string imie, string nazwisko);
        Task AddAsync(ZawodnikDto zawodnikDto);
        Task UpdAsync(ZawodnikDto zawodnikDto);
        Task DelAsync(ZawodnikDto zawodnikDto);
        Task<ZawodnikDto> GetAsync(int id);
        Task<IEnumerable<ZawodnikDto>> BrowseAll();    //all colection
    }
}
