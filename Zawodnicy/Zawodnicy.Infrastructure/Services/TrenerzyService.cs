﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Zawodnicy.Core.Domain;
using Zawodnicy.Core.Repositories;
using Zawodnicy.Infrastructure.DTO;

namespace Zawodnicy.Infrastructure.Services
{
    public class TrenerzyService : ITrenerzyService
    {
        private readonly ITrenerzyRepository _trenerzyRepository;

        public TrenerzyService(ITrenerzyRepository trenerzyRepository)
        {
            _trenerzyRepository = trenerzyRepository;
        }
        public async Task AddAsync(TrenerDto trener)
        {
            Trener t = await _trenerzyRepository.BrowseAsync(trener.Imie, trener.Nazwisko);

            if (t != null)        //trener exists!!!
            {
                string info = string.Format("Trener {0} {1} już istnieje...", trener.Imie, trener.Nazwisko);
                throw new Exception(info);
            }
            else
            {
                t = new Trener();
                t.UstawImieNazwiskoTeam(trener.Imie, trener.Nazwisko, trener.Team);
                await _trenerzyRepository.AddAsync(t);
            }


            await Task.CompletedTask;
        }

        public async Task<IEnumerable<TrenerDto>> BrowseAll()
        {
            //pobranie calej tabeli Trenerzy
            var t = await _trenerzyRepository.BrowseAll();
            //stworzenie nowej listy
            List<TrenerDto> trenerzyDto = new List<TrenerDto>();
            //przepisanie pobranej tabeli do stworzonej listy - linq
            trenerzyDto = t.Select(x => new TrenerDto()
            {
                Id = x.Id,
                Imie = x.Imie,
                Nazwisko = x.Nazwisko,
                Team = x.Team
            }
            ).ToList();

            return trenerzyDto;
        }

        public async Task<TrenerDto> BrowseAsync(string imie, string nazwisko)
        {
            Trener t = await _trenerzyRepository.BrowseAsync(imie, nazwisko);
            if (t != null)
            {
                return new TrenerDto()
                {
                    Id = t.Id,
                    Imie = t.Imie,
                    Nazwisko = t.Nazwisko,
                    Team = t.Team
                };
            }
            else
            {
                string info = String.Format("Trener {0} {1} nie istnieje...", imie, nazwisko);
                throw new Exception(info);
            }
        }

        public async Task DelAsync(TrenerDto trener)
        {
            Trener t = await _trenerzyRepository.GetAsync(trener.Id);
            if (t != null)    //trener istnieje, mozna usunac
            {
                await _trenerzyRepository.DelAsync(t);
            }
            else
            {
                string info = String.Format("Trener {0} {1} nie istnieje. Usunięcie niemożliwe!", trener.Imie, trener.Nazwisko);
                throw new Exception(info);
            }

            await Task.CompletedTask;
        }

        public async Task<TrenerDto> GetAsync(int id)
        {
            Trener t = await _trenerzyRepository.GetAsync(id);

            if (t!=null)
            {
                return new TrenerDto()
                {
                    Id = t.Id,
                    Imie = t.Imie,
                    Nazwisko = t.Nazwisko,
                    Team = t.Team
                };
            }
            else
            {
                string info = String.Format("Trener o Id={0} nie odnaleziony...", id);
                throw new Exception(info);
            }
            
        }

        public async Task UpdAsync(TrenerDto trener)
        {
            Trener t = await _trenerzyRepository.GetAsync(trener.Id);

            if (t!=null)    //Trener istnieje, mozemy updatowac
            {
                t.UstawImieNazwiskoTeam(trener.Imie, trener.Nazwisko, trener.Team);
                await _trenerzyRepository.UpdateAsync(t);
            }
            else
            {
                string info = string.Format("Trener o Id={0} nie istnieje. Update niemożliwy!", trener.Id);
                throw new Exception(info);
            }

            await Task.CompletedTask;
        }
    }
}
