﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zawodnicy.Core.Domain;
using Zawodnicy.Core.Repositories;
using Zawodnicy.Infrastructure.DTO;

namespace Zawodnicy.Infrastructure.Services
{
    public class ZawodnicyService : IZawodnicyService
    {
        private readonly IZawodnicyRepository _zawodnicyRepository;

        public ZawodnicyService(IZawodnicyRepository zawodnicyRepository)
        {
            _zawodnicyRepository = zawodnicyRepository;
        }

        public async Task AddAsync(ZawodnikDto zawodnik)
        {
            //throw new NotImplementedException();
            Zawodnik z = await _zawodnicyRepository.BrowseAsync(zawodnik.Imie,zawodnik.Nazwisko);

            if (z!=null)        //zawodnik exists!!!
            {
                string info = string.Format("Zawodnik {0} {1} już istnieje...",zawodnik.Imie,zawodnik.Nazwisko);
                throw new Exception(info);
            }
            else
            {
                z = new Zawodnik();
                z.UstawImieNazwisko(zawodnik.Imie,zawodnik.Nazwisko,zawodnik.DataUr,zawodnik.Wzrost,zawodnik.IdTrenera);
                await _zawodnicyRepository.AddAsync(z);
            }
           

            await Task.CompletedTask;
        }

        public async Task<IEnumerable<ZawodnikDto>> BrowseAll()
        {
            //throw new NotImplementedException();
            //pobranie calej tabeli Zawodnicy
            var z = await _zawodnicyRepository.BrowseAll();
            
            //stworzenie nowej listy
            List<ZawodnikDto> zawodnicyDto = new List<ZawodnikDto>();

            //przepisanie pobranej tabeli do stworzonej listy

            //ok
            //foreach (var item in z)
            //{
            //    zawodnicyDto.Add(new ZawodnikDto()
            //    {
            //        Imie = item.Imie,
            //        Nazwisko = item.Nazwisko
            //    }
            //    ); 
            //}

            //linq
            zawodnicyDto = z.Select(x => new ZawodnikDto()
            {
                Id = x.Id,
                Imie = x.Imie,
                Nazwisko = x.Nazwisko,
                DataUr=x.DataUr,
                Wzrost=x.Wzrost,
                IdTrenera=x.IdTrenera
            }
            ).ToList();

            return zawodnicyDto;

        }

        public async Task<ZawodnikDto> BrowseAsync(string imie, string nazwisko)
        {
            //throw new NotImplementedException();
            Zawodnik z= await _zawodnicyRepository.BrowseAsync(imie,nazwisko);

            if (z!=null)
            {
                return new ZawodnikDto()
                {
                    Id = z.Id,
                    Imie = z.Imie,
                    Nazwisko = z.Nazwisko,
                    DataUr = z.DataUr,
                    Wzrost = z.Wzrost,
                    IdTrenera = z.IdTrenera
                };
            }
            else
            {
                string info = string.Format("Zawodnik {0} {1} nie istnieje...", imie, nazwisko);
                throw new Exception(info);
            }


        }

        public async Task DelAsync(ZawodnikDto zawodnik)
        {
            //Takze sprawdzenie czy zawodnik istnieje zeby moc go usuwac
            Zawodnik z = await _zawodnicyRepository.GetAsync(zawodnik.Id);

            if (z!=null)    //zawodnik isrtnieje, mozna go usuwac
            {
                //z = new Zawodnik(zawodnik.Id); - tutaj mozemy calego zawodnika puscic bo przeciez istnieje
                await _zawodnicyRepository.DelAsync(z);
            }
            else
            {
                string info = string.Format("Zawodnik o Id={0} nie istnieje. Usuniecie niemożliwe!", zawodnik.Id);
                throw new Exception(info);
            }                     

            await Task.CompletedTask;
        }

        public async Task<ZawodnikDto> GetAsync(int id)
        {
            //throw new NotImplementedException();
            Zawodnik z= await _zawodnicyRepository.GetAsync(id);

            if (z!=null)
            {
                return new ZawodnikDto()
                {
                    Id = z.Id,
                    Imie = z.Imie,
                    Nazwisko = z.Nazwisko,
                    DataUr = z.DataUr,
                    Wzrost = z.Wzrost,
                    IdTrenera = z.IdTrenera
                };
            }
            else
            {
                string info = String.Format("Zawodnik o Id={0} nie odnaleziony...", id);
                throw new Exception(info);
            }
        }

        public async Task UpdAsync(ZawodnikDto zawodnik)
        {
            //throw new NotImplementedException();
            Zawodnik z = await _zawodnicyRepository.GetAsync(zawodnik.Id);

            if (z!=null)    //zawodnik istnieje, możemy updatowac
            {
                z.UstawImieNazwisko(zawodnik.Imie, zawodnik.Nazwisko, zawodnik.DataUr, zawodnik.Wzrost, zawodnik.IdTrenera);
                await _zawodnicyRepository.UpdateAsync(z);
            }
            else
            {
                string info = string.Format("Zawodnik o Id={0} nie istnieje. Update niemożliwy!", zawodnik.Id);
                throw new Exception(info);
            }
                        
            await Task.CompletedTask;
        }
    }
}
