﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ZawodnicyWebApp.Models;
using ZawodnicyWebApp.Operations;

namespace ZawodnicyWebApp.Controllers
{
    
    [Authorize(Roles = "zarzadca")]
    public class PanelAdministracyjnyController : Controller
    {
        // wykorzystanie wstrzyknietych do startup zaleznosci Identity
        private readonly UserManager<IdentityUser> _userManager;
        private AccountUsersOperation _AccountUserOperation;
        public PanelAdministracyjnyController(UserManager<IdentityUser> userManager, AccountUsersOperation accountUsersOperation)
        {
            _userManager = userManager;
            _AccountUserOperation = accountUsersOperation;
        }
 
        
        /// <summary>
        /// Obsluga userow przeniesiona do klasy AccounUsersOperation ktora zostala wstrzyknieta w startup i tu wykorzystana
        /// </summary>
        /// <returns></returns>
        //public async Task<List<UserIdent>> GetAllUsers()
        //{
        //    var Userzy = _userManager.Users.ToList();                 //lista userów Identity

        //    List<UserIdent> userzyList = new List<UserIdent>();  //lista userow moja

        //    foreach (var item in Userzy)
        //    {
        //        var roles = await _userManager.GetRolesAsync(item); //lista ról dla usera item

        //        string allroles = "";

        //        foreach (var r in roles)    //dodanie listy ról do wynikowego napisu
        //        {
        //            if (allroles=="")
        //            {
        //                allroles = String.Format("{0}{1}{2}", allroles, " ", r);
        //            }
        //            else
        //            {
        //                allroles = String.Format("{0}{1}{2}", allroles, " / ", r);
        //            }
                    
        //        }

        //        userzyList.Add(new UserIdent()
        //        {
        //            Name = item.UserName, //u.UserName,

        //            Role = allroles
        //        });
        //    }

        //    return userzyList;
        //} 

        // GET: PanelAdministracyjnyController
        public async Task<ActionResult> Index()
        {
            List<UserIdent> lista = new List<UserIdent>();

            //lista = await GetAllUsers();
            lista = await _AccountUserOperation.GetAllUsers();

            return View(lista);
        }

        // GET: PanelAdministracyjnyController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PanelAdministracyjnyController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PanelAdministracyjnyController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PanelAdministracyjnyController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PanelAdministracyjnyController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PanelAdministracyjnyController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PanelAdministracyjnyController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
