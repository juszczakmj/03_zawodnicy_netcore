﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace ZawodnicyWebApp.Controllers
{
    public class SandboxJWTokenSchematController : Controller
    {
        /// <summary>
        /// !!!
        /// CZYSTY MODELOWY SCHEMAT GENEROWANIA JSON WEB TOKEN I UŻYCIA W WYWOŁANIACH
        /// DLATEGO BEZ ODWOLAN DO APPSEETINGS.JSON I GENEROWANIE TOKENU BEZPOSREDNIO TUTAJ
        /// DOCELOWO GENEROWANIE TOKENU W INNEJ KLASIE (I POBRANIE DANYCH Z APPSETTINGS.JSON TEZ)
        /// </summary>
        /// <returns></returns>
        private string GenerateJSONWebToken()   //przenieść do Models i stworzyć klasę
        {

            //SecretKey musi być zgodny z tym w Api (REST)
            //issuer, audience maja byc zgodne takze z tym w Api (REST)
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Xxxxxxxxxxxxxxxxxxxxx"));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);


            //var claims = new[] {
            //    new Claim("Name", "Mirek"),
            //    new Claim(JwtRegisteredClaimNames.Email, "juszczakmj@gmail.com"),
            //};


            var token = new JwtSecurityToken(
                issuer: "http://localhost:5000/",
                audience: "http://localhost:5000/",
                expires: DateTime.Now.AddHours(3),
                signingCredentials: credentials     //wygenerowany klucz na podst SecureKey
                                                    //claims: claims
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        public async Task<IActionResult> Index()
        {

            //var token = new Models.JWToken();

            string apiResponse;

            using (var httpClient = new HttpClient())
            {
                var tokenString = GenerateJSONWebToken();

                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", tokenString);
                //Tutaj juz wywolanie akcji kontrolera w Api
                using (var response = await httpClient.GetAsync("http://localhost:5000/SandboxTajne"))
                {
                    apiResponse = await response.Content.ReadAsStringAsync();

                }
            }

            ViewData["odpowiedz"] = apiResponse; //można przekazać tak parametry na szybko zamiast całego modelu
            return View();
        }
    }
}
