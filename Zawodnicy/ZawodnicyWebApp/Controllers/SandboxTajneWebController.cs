﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using ZawodnicyWebApp.Models;

namespace ZawodnicyWebApp.Controllers
{
    public class SandboxTajneWebController : Controller
    {
        /// <summary>
        /// SANDBOX CONTROLLER OPARTY NA SandboxJWTokenSchematController
        /// ALE JUZ Z METODA GENEROWANIA TOKENU WYNIESIONA DO INNEJ KLASY A URL-e POBIERANE
        /// Z APPSETTINGS - TEZ POPRZEZ ODREBNA KLASE JWToken 
        /// Klasa JWToken wstrzyknieta do Startup
        /// </summary>

        //Pobranie wstrzykniętych w startup zależnośći
        public IConfiguration Configuration;

        private JWToken _JWToken;
        public SandboxTajneWebController(IConfiguration configuration, JWToken jWToken)
        {
            Configuration = configuration;

            _JWToken = jWToken;
        }
        ///// 


        public ContentResult OnGet()
        {
            //przy zmianie URL wystarczy wskazac inny klucz w appsettings niz RestApiUrl:HostUrl
            //lub w appsettings.js podmieniać HostUrl
            var myRestUrl = Configuration["RestApiUrl:HostUrl"];

            return Content($"{myRestUrl}");
        }
        // --- Koniec pobrania danych z appsettings ---


        //zmienna do przechowywania URL do REST pobranego metodą OnGet()
        private string _restpath;
        //nazwa wywoływanego contorllera
        private string _controllername = "SandboxTajne";

        public async Task<IActionResult> Index()
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            string apiResponse;

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                using (var response = await httpClient.GetAsync($"{_restpath}"))
                {
                    apiResponse = await response.Content.ReadAsStringAsync();
                    
                }
            }

            ViewData["odpowiedz"]=apiResponse; //można przekazać tak parametry na szybko zamiast całego modelu
            return View();
        }
    }
}
