﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ZawodnicyWebApp.Controllers
{
    [Authorize(Roles = "wazny,zarzadca")]
    public class TajneController : Controller
    {
        // GET: TajneController
        public ActionResult Index()
        {
            return View();
        }

        // GET: TajneController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TajneController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TajneController/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TajneController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TajneController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TajneController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TajneController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
