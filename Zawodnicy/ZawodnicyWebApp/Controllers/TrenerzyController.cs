﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ZawodnicyWebApp.Models;

namespace ZawodnicyWebApp.Controllers
{
    public class TrenerzyController : Controller
    {
        //Pobranie wstrzyknietych w stratup zaleznosci
        public IConfiguration Configuration;
        private JWToken _JWToken;
        public TrenerzyController(IConfiguration confiruration, JWToken jwToken)
        {
            Configuration = confiruration;
            _JWToken = jwToken;
        }

        //Pobranie do klasy danych z appsettings.json
        public ContentResult OnGet()
        {
            //przy zmianie URL wystarczy wskazac inny klucz w appsettings niz RestApiUrl:HostUrl
            //lub w appsettings.js podmieniać HostUrl
            var myRestUrl = Configuration["RestApiUrl:HostUrl"];

            return Content($"{myRestUrl}");
        }
        // --- Koniec pobrania danych z appsettings ---
 
        //zmienna do przechowywania URL do REST pobranego metodą OnGet()
        private string _restpath;
        //nazwa wywoływanego contorllera
        private string _controllername = "trenerzyApi";

        // GET: TrenerzyController
        public async Task<ActionResult> Index()
        {
            _restpath = $"{OnGet().Content}{_controllername}";
            List<Trener> trenerzyList = new List<Trener>();
                       
            using (var httpClient = new HttpClient())
            {
                //Dodanie przed using naglowka autoryzacji z tokenem
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                using (var response = await httpClient.GetAsync($"{_restpath}"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    trenerzyList = JsonConvert.DeserializeObject<List<Trener>>(apiResponse);
                }
            }

                return View(trenerzyList);
        }

        // GET: TrenerzyController/Details/{id}
        public async Task<ActionResult> Details(int id)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            Trener t = new Trener();
            using (var httpClient = new HttpClient())
            {
                //Dodanie przed using naglowka autoryzacji z tokenem
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                using (var response = await httpClient.GetAsync($"{_restpath}/{id}"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    t = JsonConvert.DeserializeObject<Trener>(apiResponse);
                }
            }
            return View(t);
        }

        // GET: TrenerzyController/Create
        public async Task<ActionResult> Create()
        {
            Trener t = new Trener();
            return View(t);
        }

        // POST: TrenerzyController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Create(IFormCollection collection)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            Trener t = new Trener();
            t.Imie = collection["Imie"];
            t.Nazwisko = collection["Nazwisko"];
            t.Team = collection["Team"];

            try
            {
                using (var httpClient = new HttpClient())
                {
                    string jsonStr = System.Text.Json.JsonSerializer.Serialize(t);
                    var content = new StringContent(jsonStr, Encoding.UTF8, "application/json");

                    //Dodanie przed using naglowka autoryzacji z tokenem
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                    using (var response = await httpClient.PostAsync($"{_restpath}", content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        t = JsonConvert.DeserializeObject<Trener>(apiResponse);
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TrenerzyController/Edit/{id}
        //Brak deklaracji [HttpPost] lub innej metody - Akcja przekazana do metody Details w biezacym kontrolerze
        public async Task<ActionResult> Edit(int id)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            Trener t = new Trener();

            using (var httpClient = new HttpClient())
            {
                //Dodanie przed using naglowka autoryzacji z tokenem
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                using (var response = await httpClient.GetAsync($"{_restpath}/{id}"))
                {                    
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    t = JsonConvert.DeserializeObject<Trener>(apiResponse);
                }
            }

            return View(t);
        }

        // POST: TrenerzyController/Edit/5
        // Deklaracja [HttpPost] Akcja przekazana do metody Details w REST (tu Post)
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Edit(int id, IFormCollection collection)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            //Tu przesłanie w ciele Body
            Trener t = new Trener();
            t.Id = Convert.ToInt32(collection["Id"]);
            t.Imie = collection["Imie"];
            t.Nazwisko = collection["Nazwisko"];
            t.Team = collection["Team"];

            try
            {
                using (var httpClient = new HttpClient())
                {
                    string jsonStr = System.Text.Json.JsonSerializer.Serialize(t);
                    var content = new StringContent(jsonStr, Encoding.UTF8, "application/json");

                    //Dodanie przed using naglowka autoryzacji z tokenem
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                    using (var response = await httpClient.PutAsync($"{_restpath}/{id}", content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        t = JsonConvert.DeserializeObject<Trener>(apiResponse);
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TrenerzyController/Delete/{id}
        // Pobranie trenera i zatweirdzenie jego usunięcia
        public async Task<ActionResult> Delete(int id)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            Trener t = new Trener();
            using (var httpClient = new HttpClient())
            {
                //Dodanie przed using naglowka autoryzacji z tokenem
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                using (var response = await httpClient.GetAsync($"{_restpath}/{id}"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    t = JsonConvert.DeserializeObject<Trener>(apiResponse);
                }
            }
            return View(t);
        }

        // POST: TrenerzyController/Delete/{id}
        //Wysłanie zadania usuniecia trenera do REST Zawodnicy.API
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Delete(int id, IFormCollection collection)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            //collection tu nie wykorzystujemy
            //Potem tak naprawde juz tylko Id bedzie potrzebne ale tu wywolamy calosc trenara

            try
            {
                using (var httpClient = new HttpClient())
                {
                    //Dodanie przed using naglowka autoryzacji z tokenem
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                    using (var response = await httpClient.DeleteAsync($"{_restpath}/{id}"))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        //z = JsonConvert.DeserializeObject<Zawodnik>(apiResponse);
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
