﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ZawodnicyWebApp.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Configuration;
using System.Dynamic;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Globalization;
using Microsoft.AspNetCore.Authorization;

namespace ZawodnicyWebApp.Controllers
{
    public class ZawodnicyController : Controller
    {

        //Pobranie wstrzyknietych w stratup zaleznosci
        public IConfiguration Configuration;

        private JWToken _JWToken;

        public ZawodnicyController(IConfiguration confiruration, JWToken jwToken)
        {
            Configuration = confiruration;
            _JWToken = jwToken;
        }

        //Pobranie do klasy danych z appsettings.json

        public ContentResult OnGet()
        {
            //przy zmianie URL wystarczy wskazac inny klucz w appsettings niz RestApiUrl:HostUrl
            //lub w appsettings.js podmieniać HostUrl
            var myRestUrl = Configuration["RestApiUrl:HostUrl"];
            
            return Content($"{myRestUrl}");
        }
        // --- Koniec pobrania danych z appsettings ---

        //zmienna do przechowywania URL do REST pobranego metodą OnGet()
        private string _restpath;
        //nazwa wywoływanego contorllera
        private string _controllername = "zawodnicyApi";


        // GET: ZawodnicyController
        public async Task<ActionResult> Index()
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            List<Zawodnik> zawodnicyList = new List<Zawodnik>();

            using (var httpClient = new HttpClient())
            {
                //Dodanie przed using naglowka autoryzacji z tokenem
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                using (var response = await httpClient.GetAsync($"{_restpath}"))
                { 
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    zawodnicyList = JsonConvert.DeserializeObject<List<Zawodnik>>(apiResponse);
                }
            }

                return View(zawodnicyList);
        }

        // GET: ZawodnicyController/Details/5
        public async Task<ActionResult> Details(int id)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            Zawodnik z = new Zawodnik();
            using (var httpClient = new HttpClient())
            {
                //Dodanie przed using naglowka autoryzacji z tokenem
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                using (var response = await httpClient.GetAsync($"{_restpath}/{id}"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    z = JsonConvert.DeserializeObject<Zawodnik>(apiResponse);
                }
            }
            return View(z);
        }

        // GET: ZawodnicyController/Create
        public ActionResult Create()
        {
            Zawodnik z = new Zawodnik();
            return View(z);
        }

        // POST: ZawodnicyController/Create
        // Nazwa metody jest zgodna z acion formularza Views Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Create(IFormCollection collection)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            Zawodnik z = new Zawodnik();
            //z.Id = Convert.ToInt32(collection["Id"]); Zostanie utworzone automatycznie
            z.Imie = collection["Imie"];
            z.Nazwisko = collection["Nazwisko"];
            z.Wzrost = float.Parse(collection["Wzrost"], CultureInfo.InvariantCulture.NumberFormat);
            z.DataUr = Convert.ToDateTime(collection["DataUr"]);
            z.IdTrenera = Convert.ToInt32(collection["IdTrenera"]);

            try
            {

                using (var httpClient = new HttpClient())
                {
                    string jsonStr = System.Text.Json.JsonSerializer.Serialize(z);
                    var content = new StringContent(jsonStr, Encoding.UTF8, "application/json");

                    //Dodanie przed using naglowka autoryzacji z tokenem
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                    using (var response = await httpClient.PostAsync($"{_restpath}", content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        z = JsonConvert.DeserializeObject<Zawodnik>(apiResponse);
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ZawodnicyController/Edit/{id}
        //Brak deklaracji [HttpPost] lub innej metody - Akcja przekazana do metody Details w biezacym kontrolerze
        public async Task<ActionResult> Edit(int id)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            Zawodnik z = new Zawodnik();

            using (var httpClient = new HttpClient())
            {
                //Dodanie przed using naglowka autoryzacji z tokenem
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                using (var response = await httpClient.GetAsync($"{_restpath}/{id}"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    z = JsonConvert.DeserializeObject<Zawodnik>(apiResponse);
                }
            }

            return View(z);
        }

        // POST: ZawodnicyController/Edit/{id}
        // Deklaracja [HttpPost] Akcja przekazana do metody Details w REST (tu Post)
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Edit(int id, IFormCollection collection)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            //Tu przesłanie w ciele Body

            Zawodnik z = new Zawodnik();
            z.Id=Convert.ToInt32(collection["Id"]);
            z.Imie=collection["Imie"];
            z.Nazwisko= collection["Nazwisko"];
            z.Wzrost = float.Parse(collection["Wzrost"], CultureInfo.InvariantCulture.NumberFormat);
            z.DataUr = Convert.ToDateTime(collection["DataUr"]);
            z.IdTrenera = Convert.ToInt32(collection["IdTrenera"]);

            try
            {
                //=collection

                using (var httpClient = new HttpClient())
                {
                    string jsonStr = System.Text.Json.JsonSerializer.Serialize(z);
                    var content = new StringContent(jsonStr, Encoding.UTF8, "application/json");

                    //Dodanie przed using naglowka autoryzacji z tokenem
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                    using (var response = await httpClient.PutAsync($"{_restpath}/{id}", content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        z = JsonConvert.DeserializeObject<Zawodnik>(apiResponse);
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ZawodnicyController/Delete/{id}  
        // Pobranie zawodnika i zatweirdzenie jego usunięcia
        public async Task<ActionResult> Delete(int id)
        {
            _restpath = $"{OnGet().Content}{_controllername}";

            Zawodnik z = new Zawodnik();
            using (var httpClient = new HttpClient())
            {
                //Dodanie przed using naglowka autoryzacji z tokenem
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                using (var response = await httpClient.GetAsync($"{_restpath}/{id}"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    z = JsonConvert.DeserializeObject<Zawodnik>(apiResponse);
                }
            }

            return View(z);
        }

        // POST: ZawodnicyController/Delete/{id}
        //Wysłanie zadania usuniecia zawodnika do REST Zawodnicy.API
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Delete(int id, IFormCollection collection)
        {

            _restpath = $"{OnGet().Content}{_controllername}";

            //collection tu nie wykorzystujemy
            //Potem tak naprawde juz tylko Id bedzie potrzebne ale tu wywolamy calosc zawodnika

            //Zawodnik z = new Zawodnik();       

            try
            {
                using (var httpClient = new HttpClient())
                {
                    //Dodanie przed using naglowka autoryzacji z tokenem
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _JWToken.TokenString);
                    using (var response = await httpClient.DeleteAsync($"{_restpath}/{id}"))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        //z = JsonConvert.DeserializeObject<Zawodnik>(apiResponse);
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
