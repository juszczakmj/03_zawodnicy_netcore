﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZawodnicyWebApp.Models
{
    public class RegisterVM:LoginVM
    {
        [Display(Name = "Podaj hasło ponownie")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
