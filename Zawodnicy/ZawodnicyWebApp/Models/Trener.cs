﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZawodnicyWebApp.Models
{
    public class Trener
    {
        public Trener()
        {

        }
        public Trener(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Team { get; set; }

    }
}
