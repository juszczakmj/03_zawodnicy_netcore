﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZawodnicyWebApp.Models
{
    /// <summary>
    /// klasa opisuje zawodnika z tabel ASP .NET Identity
    /// </summary>
    public class UserIdent
    {
        public string Name { get; set; }        //Name usera
        public string Role { get; set; }        //Rola usera
        public string Email { get; set; }       //Email usera
        public string PhoneNumber { get; set; }        //Telefon usera

    }
}
