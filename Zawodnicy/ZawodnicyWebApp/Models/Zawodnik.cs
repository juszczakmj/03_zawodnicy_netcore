﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZawodnicyWebApp.Models
{
    public class Zawodnik
    {
        public Zawodnik()
        {

        }
        public Zawodnik(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public DateTime DataUr { get; set; }      
        public float Wzrost { get; set; }
        public int IdTrenera { get; set; }        //powiazanie z Trenerami

    }
}
