﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using ZawodnicyWebApp.Models;

namespace ZawodnicyWebApp.Operations
{
    /// <summary>
    /// Klasa obsuguje dane userów z bazy ASP .Net Identity
    /// </summary>
    public class AccountUsersOperation
    {
        // wykorzystanie wstrzyknietych do startup zaleznosci Identity
        private readonly UserManager<IdentityUser> _userManager;
        public AccountUsersOperation(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<List<UserIdent>> GetAllUsers()
        {
            var Userzy = _userManager.Users.ToList();                 //lista userów Identity

            List<UserIdent> userzyList = new List<UserIdent>();  //lista userow moja

            foreach (var item in Userzy)
            {
                var roles = await _userManager.GetRolesAsync(item); //lista ról dla usera item

                string allroles = "";

                foreach (var r in roles)    //dodanie listy ról do wynikowego napisu
                {
                    if (allroles == "")
                    {
                        allroles = String.Format("{0}{1}{2}", allroles, " ", r);
                    }
                    else
                    {
                        allroles = String.Format("{0}{1}{2}", allroles, " / ", r);
                    }

                }

                userzyList.Add(new UserIdent()
                {
                    Name = item.UserName, 
                    PhoneNumber=item.PhoneNumber,
                    Email=item.Email,
                    Role = allroles
                });
            }

            return userzyList;
        }
    }
}
